<?php
/**
 * @file
 * coderdojo_views_dojo_list.features.inc
 */

/**
 * Implements hook_views_api().
 */
function coderdojo_views_dojo_list_views_api() {
  return array("version" => "3.0");
}
